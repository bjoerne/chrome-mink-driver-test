Feature: Radio buttons
    In order to check if radio buttons are working in behat-chrome-extension
    As a developer
    I need to execute these tests

    Scenario: Select radio button with letter
        Given I go to "radio_button_with_value_a.html"
        When I select "a" from "radio_letter"
        Then the "radio_letter" field should contain "a"

    Scenario: Select radio button with digit
        Given I go to "radio_button_with_value_1.html"
        When I select "1" from "radio_letter"
        Then the "radio_letter" field should contain "1"
